package util;

import com.nespresso.exercise.electric_trip.Card;

public class ParserCard {

	private static final String CARDSPLITER = "-";

	public static Card parseCardDescription(String cardDescription) {

		String citySource = cardDescription.split(CARDSPLITER)[0];
		String distance = cardDescription.split(CARDSPLITER)[1];
		String cityDestination = cardDescription.split(CARDSPLITER)[2];

		return new Card(citySource, distance, cityDestination);

	}
}
