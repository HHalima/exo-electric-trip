package com.nespresso.exercise.electric_trip;

import java.util.Random;

public class Participant {
	private int id;
	private Card cardOfTrip;
	private  Car carOfTrip;
	private String currentCity;

	public Participant(Card cardOfTrip, Car carOfTrip) {
		this.cardOfTrip=cardOfTrip;
		this.carOfTrip=carOfTrip;
		this.currentCity=cardOfTrip.cityDepart();		
	}
	
	public Car ParticipantCar() {
		return this.carOfTrip;
	}
	
	public Card ParticipantCard() {
		return this.cardOfTrip;
	}
	
	public void nextDestination( String nextDestination) {
		this.currentCity=nextDestination;
	}
	
	public String participantLocation() {
		return currentCity;
	}
	
	public int participantId() {
		return 0;
	}
	
	

}
