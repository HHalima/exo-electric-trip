package com.nespresso.exercise.electric_trip;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Car {
	
	private int batterySize;
	private int currentBatterySize;
	private int lowSpeedPerformance;
	private int highSpeedPerformance;
     
     
	public Car(int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
		this.batterySize = batterySize;
		this.lowSpeedPerformance = lowSpeedPerformance;
		this.highSpeedPerformance = highSpeedPerformance;
	}

	public int changeBatterySize(int distance) {		
		int consumedBatterySize=batterySize-(distance/lowSpeedPerformance);
		if (consumedBatterySize>0) {
			currentBatterySize=consumedBatterySize;
		}else {
			currentBatterySize=0;
		}
		return (currentBatterySize*100)/batterySize;
	}
	
	public String currentBatterySize() {
		return BigDecimal.valueOf((currentBatterySize*100.0)/batterySize).setScale(0, RoundingMode.HALF_UP)+"%";
	}
}
