package com.nespresso.exercise.electric_trip;

import java.util.HashMap;
import java.util.Map;

import util.ParserCard;



public class ElectricTrip {
	
  Map<String, Card> listOfCards= new HashMap<String, Card>();
  Map<Integer, Participant> listOfParticipants=new HashMap<Integer, Participant>();
  
	public ElectricTrip(String cardDescription) {
		Card tripCard=ParserCard.parseCardDescription(cardDescription);
		listOfCards.put(tripCard.cityDepart(), tripCard);
		System.out.println("");
	}

	public int startTripIn(String cityDepart, int batterySize, int lowSpeedPerformance, int highSpeedPerformance) {
		Car tripCar=new Car(batterySize, highSpeedPerformance, highSpeedPerformance);
		Card tripCard =listOfCards.get(cityDepart);
		Participant participant= new Participant(tripCard, tripCar);
		listOfParticipants.put(participant.participantId(), participant);
		return participant.participantId();
	}

	public void go(int participantId) {
		Participant participant=listOfParticipants.get(participantId);	
		if (participant.ParticipantCar().changeBatterySize(participant.ParticipantCard().distance())>0) {
			participant.nextDestination(participant.ParticipantCard().cityDestination());
		}
	}
		
	public String locationOf(int participantId) {
		Participant participant=listOfParticipants.get(participantId);	
		return participant.participantLocation();
	}

	public String chargeOf(int participantId) {
		Participant participant=listOfParticipants.get(participantId);
		return participant.ParticipantCar().currentBatterySize();
	}

	public void sprint(int participantId) {
		// TODO Auto-generated method stub
		
	}

	public void charge(int id, int i) {
		// TODO Auto-generated method stub
		
	}

}
