package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;

public class Card{
	
	List<String> traject =new ArrayList<String>();

	public Card(String cityDepart, String distance, String cityDestination) {
		traject.add(0, cityDepart);
		traject.add(1,distance);
		traject.add(2,cityDestination);
	}
	
	public String cityDepart() {		
		return traject.get(0);	
	}
	public String cityDestination() {
		return traject.get(2);	
	}
	
	public int distance() {
		return Integer.valueOf(traject.get(1));
	}
	

}
